﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Finternational.Data.Importer
{
    public class Player
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("position")]
        public string Position { get; set; }


        public int PositionId { get { return (int)Enum.Parse(typeof(Position), this.Position); } }

    }

    public enum Position
    {
        Goalkeeper = 1,
        Defender = 2,
        Midfielder = 3,
        Attacker = 4,
    }
}
