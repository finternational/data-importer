﻿using Finternational.Data.Importer;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;


using(SqlConnection connection = new SqlConnection(""))
{
    connection.Open();
    var ids = new List<int>() { 26, 20, 1, 6, 1530, 5529, 29, 3, 21, 2382, 10, 2, 25, 1504, 22, 12, 16, 31, 1118, 24, 27, 1569, 23, 13, 14, 17, 9, 15, 28, 7, 2384, 767 };
    HttpClient client = new HttpClient();
    client.DefaultRequestHeaders.Add("x-rapidapi-host", "v3.football.api-sports.io");
    client.DefaultRequestHeaders.Add("x-rapidapi-key", "");

    foreach (int id in ids)
    {
        HttpResponseMessage response = await client.GetAsync($"https://v3.football.api-sports.io/players/squads?team={id}");
        var players = JsonConvert.DeserializeObject<GetPlayers>(await response.Content.ReadAsStringAsync());

        foreach(var player in players.Response[0].Players)
        {
            using(var command = connection.CreateCommand())
            {
                try
                {
                    command.CommandText = $"INSERT INTO COMPETITION.player(Id, FirstName, LastName, Country, Position, Name, ExternalId) values " +
                        $"({player.Id}, '', @playerName, {id}, {player.PositionId}, @playerName, {player.Id})";


                    command.Parameters.Add("@playerName", SqlDbType.NVarChar).Value = player.Name;
                    await command.ExecuteNonQueryAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(command.CommandText);
                }
            }
        }
    }
}