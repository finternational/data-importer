﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Finternational.Data.Importer
{
    public class PlayerResponse
    {
        [JsonPropertyName("players")]
        public List<Player> Players { get; set; }
    }
}
