﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Finternational.Data.Importer
{
    public class GetPlayers
    {
        [JsonPropertyName("response")]
        public List<PlayerResponse> Response { get; set; }
    }
}
